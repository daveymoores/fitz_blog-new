<?php
/**
 * Default template for displaying Footer
 * @package sampression framework v 1.0
 * @theme naya 1.0
 */
if ( ! defined( 'ABSPATH' ) ) exit( 'restricted access' );
?>
        <?php
        /** 
        * sampression_before_footer hook
        */
        do_action('sampression_before_footer'); 
        ?>
    <div id="fitz_input">
        <form action="//fitzcarraldoeditions.us8.list-manage.com/subscribe/post?u=5337be9695afc83be35de5125&amp;id=f172142be6" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div class="row">
                <div class="large-6 large-centered columns">

                        <img src="<?php bloginfo('template_directory'); ?>/images/fitz-mark-txt_white.png" alt="Fitz Carraldo Editions">
                        <span class="signup">Sign up to our newsletter</span>

                </div>
            </div>
            <div class="row">
                <div class="large-6 large-centered columns">
                    <div class="row">
                        <div class="large-8 columns">
                            <input type="email" name="EMAIL" required placeholder="your email">
                        </div>
                        <div class="large-4 columns">
                            <input type="submit" class="button" value="Subscribe">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="lowerMenu">
        <img src="<?php bloginfo('template_directory'); ?>/images/fitz-mark.png" alt="Fitz Carraldo Editions mark">
        <div class="row">
            <div class="large-12 large-center columns">
                <div class="center">
                    <ul class="lower_menu">
                        <li><a href="https://fitzcarraldoeditions.co.uk/about">about</a></li>
                        <li><a href="https://fitzcarraldoeditions.co.uk/distribution">sales &amp; distribution</a></li>
                        <li class="last"><a href="">catalogue</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="row">
            <div class="large-2 small-6 columns">
                <ul class="address">
                    <li><img src="<?php bloginfo('template_directory'); ?>/images/fitz-mark-txt.png" alt="Fitz Carraldo Editions"></li>
                    <li>243 Knightsbridge</li>
                    <li>London SW7 1DN</li>
                    <li><a href="mailto:info@fitzcarraldoeditions.com">info@fitzcarraldoeditions.com</a></li>
                </ul>
            </div>

            <div class="large-3 small-6 columns regtext">
                <ul>
                  <li><a href="https://fitzcarraldoeditions.co.uk/pivacy">FAQ's &amp; Privacy</a></li>
              </ul>
            </div>
        </div>
    </footer>      
        <?php
        /** 
        * sampression_after_footer hook
        */
        do_action('sampression_after_footer'); 
        ?>
    </div>
    <!--/#inner-wrapper-->
</div>
<!--/#wrapper-->
<!--[if lt IE 9]>
    <script src="<?php echo SAM_FW_JS_URL; ?>/selectivizr.js?v=1.0.1"></script>
<![endif]-->
<!-- Prompt IE 6 users to install Chrome Frame -->
<!--[if lt IE 7 ]>
	<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
    <script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script>
	<script src="<?php echo get_template_directory_uri(); ?>/lib/js/dd_belatedpng.js"></script>
	<script> DD_belatedPNG.fix('img, .png-bg'); </script>
<![endif]-->
<?php 
/** 
* sampression_before_body_close hook
*/
do_action('sampression_before_body_close'); 
?>
<?php wp_footer();  ?>
</body>
</html>